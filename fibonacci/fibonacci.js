
function fibonacci(n) {
    let arrayFibonacci = [0, 1];
    for (var i = 2; i < n; i++) {
      arrayFibonacci[i] = arrayFibonacci[i-1] + arrayFibonacci[i-2];
    }
    return arrayFibonacci[n-1];
};
